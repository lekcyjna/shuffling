import random
import matplotlib.pyplot as plt

def suffleMy(T):
    D={}
    for i in T:
        if i in D:
            D[i]+=1;
        else:
            D[i]=1;
    Out=[]
    dl=len(T)
    for i in range(dl):
        r=random.random()
        s=0
        for j in D:
            if (s+D[j])/(dl-i)>=r:
                Out.append(j)
                D[j]-=1
                if D[j]<=0:
                    del D[j]
                break
            else:
                s+=D[j]
    return Out

def testBuildin(T,n):
    dictOfPerm={}
    for i in range(n):
        random.shuffle(T)
        perm=str(T)
        if perm in dictOfPerm:
            dictOfPerm[perm]+=1;
        else:
            dictOfPerm[perm]=1

    print(dictOfPerm)
    plt.bar(range(len(list(dictOfPerm.keys()))), dictOfPerm.values())
    plt.xticks(range(len(list(dictOfPerm.keys()))), dictOfPerm.keys(), rotation="vertical")
    plt.draw()

def testMy(T,n):
    dictOfPerm={}
    for i in range(n):
        perm=str(suffleMy(T))
        if perm in dictOfPerm:
            dictOfPerm[perm]+=1;
        else:
            dictOfPerm[perm]=1

    print(dictOfPerm)
    plt.bar(range(len(list(dictOfPerm.keys()))), dictOfPerm.values())
    plt.xticks(range(len(list(dictOfPerm.keys()))), dictOfPerm.keys(), rotation="vertical")
    plt.draw()
            
#test of normal permutation
#testMy([1,2,3,4],10000000)

#test of permutation with reapets
plt.subplot(1,2,1)
testMy([1,2,2,3], 10000000)
print("==================================================")
plt.subplot(1,2,2)
testBuildin([1,2,2,3], 10000000)
plt.show()
