import os,sys
import subprocess as sub

def testIfProgramRun(cmd, errorMsg, capture_output=False, outDir=None, numberOfFiles=1):
    if outDir is not None:
        for path in os.listdir(outDir):
            os.remove(os.path.join(outDir,path))
    try:
        out=sub.run(cmd, capture_output=True, check=True)
    except sub.CalledProcessError as e:
        print(errorMsg)
        print(e.stdout)
        print(e.stderr)
        print(e)
        return 0
    if capture_output and out.stdout=="":
        print(errorMsg,":","Empty stdout")
        return 0
    if outDir is not None and len(os.listdir(outDir))!=numberOfFiles:
        print(errorMsg,": Wrong number of files")
        print("Expected:", numberOfFiles,"There is:",len(os.listdir(outDir)))
        return 0
    return 1



def runTests():
    sum=0
    all=0
    sum+=testIfProgramRun(["python","../../Code/main.py", "-h"],"Error while checking help.", capture_output=True)
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test1.fna", "--nucAll", "-od","../Wyj"],
            "Error while checking nucAll. Test 1.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test1.fna", "--nucPos", "-od","../Wyj"],
            "Error while checking nucPos. Test 1.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test1.fna", "--codons", "-od","../Wyj"],
            "Error while checking codons. Test 1.", outDir ="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test1.fna", "--aminoacids", "-od","../Wyj"],
            "Error while checking aminoacids. Test 1.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test2.fna", "--nucAll", "-od","../Wyj"],
            "Error while checking nucAll. Test 2.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test2.fna", "--nucPos", "-od","../Wyj"],
            "Error while checking nucPos. Test 2.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test2.fna", "--codons", "-od","../Wyj"],
            "Error while checking codons. Test 2.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test2.fna", "--aminoacids", "-od","../Wyj"],
            "Error while checking aminoacids. Test 2.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test2.fna", "--nucAll", "-od","../Wyj", "-v"],
            "Error while checking nucAll verbose. Test 2.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test2.fna", "--nucPos", "-od","../Wyj", "-v"],
            "Error while checking nucPos verbose. Test 2.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test2.fna", "--codons", "-od","../Wyj", "-v"],
            "Error while checking codons verbose. Test 2.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test2.fna", "--aminoacids", "-od","../Wyj", "-v"],
            "Error while checking aminoacids verbose. Test 2.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test1.fna", "--nucAll", "-od","../Wyj", "-s","-e"],
            "Error while checking start and stop nucAll. Test 1.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test1.fna", "--nucPos", "-od","../Wyj", "-s","-e"],
            "Error while checking start and stop nucPos. Test 1.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test1.fna", "--codons", "-od","../Wyj", "-s","-e"],
            "Error while checking start and stop codons. Test 1.", outDir ="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test1.fna", "--aminoacids", "-od","../Wyj", "-s","-e"],
            "Error while checking start and stop aminoacids. Test 1.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test2.fna", "--nucAll", "-od","../Wyj","-t" ],
            "Error while checking distinct nucAll. Test 2.", outDir="../Wyj", numberOfFiles=2)
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test2.fna", "--nucPos", "-od","../Wyj","-t"],
            "Error while checking distinct nucPos. Test 2.", outDir="../Wyj", numberOfFiles=2)
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test2.fna", "--codons", "-od","../Wyj","-t"],
            "Error while checking distinct codons. Test 2.", outDir="../Wyj",numberOfFiles=2)
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test2.fna", "--aminoacids", "-od","../Wyj","-t"],
            "Error while checking distinct aminoacids. Test 2.", outDir="../Wyj", numberOfFiles=2)
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test2.fna", "--nucAll", "-od","../Wyj", "-pr"],
            "Error while checking nucAll with python PRNG. Test 2.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test2.fna", "--nucPos", "-od","../Wyj", "-pr"],
            "Error while checking nucPos with python PRNG. Test 2.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test2.fna", "--codons", "-od","../Wyj", "-pr"],
            "Error while checking codons with python PRNG. Test 2.", outDir="../Wyj")
    all+=1
    sum+=testIfProgramRun(["python","../../Code/main.py", "../Files/test2.fna", "--aminoacids", "-od","../Wyj", "-pr"],
            "Error while checking aminoacids with python PRNG. Test 2.", outDir="../Wyj")
    all+=1
    print("Tests passed:",sum)
    print("All tests:",all)

if __name__=="__main__":
    runTests()
