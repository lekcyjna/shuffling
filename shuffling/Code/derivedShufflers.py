import itertools as iter
import random
import Shuffler 
import SGC

class NucAllShuffler(Shuffler.Shuffler):
    def _shufflingAlg(self, seq):
        return "".join(random.sample(seq,len(seq)))


class NucPosShuffler(Shuffler.Shuffler):
    def _shufflingAlg(self, seq):
        pos1=list(seq[::3])
        pos2=list(seq[1::3])
        pos3=list(seq[2::3])
        random.shuffle(pos1)
        random.shuffle(pos2)
        random.shuffle(pos3)
        seqShuffled=zip(pos1,pos2,pos3)
        seqShuffled=iter.chain.from_iterable(seqShuffled)
        return "".join(seqShuffled)

class CodonShuffler(Shuffler.Shuffler):
    def _shufflingAlg(self,seq):
        seqToShuffle=[seq[i:i+3] for i in range(0,len(seq),3)]
        random.shuffle(seqToShuffle)
        seqShuffled=iter.chain.from_iterable(seqToShuffle)
        return "".join(seqShuffled)

class AminoacidShuffler(Shuffler.Shuffler):
    def _shufflingAlg(self,seq):
        seqShuffled=[]
        for i in range(0,len(seq),3):
            aa=SGC.codonToAmin[str(seq[i:i+3])]
            numberOfSynCodons=len(SGC.aminToCodons[aa])
            r=random.randint(0,numberOfSynCodons-1)
            seqShuffled.append(tuple(SGC.aminToCodons[aa])[r])
        seqShuffled=iter.chain.from_iterable(seqShuffled)
        return "".join(seqShuffled)
            
