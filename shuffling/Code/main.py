import os
import random
import argparse 
import derivedShufflers as dS
import readSequenceFunctions as rsf


def prepareOutDir(arg):
    arg.out_dir=os.path.abspath(arg.out_dir+"/")
    if os.path.isdir(arg.out_dir):
        return
    if os.path.exists(arg.out_dir):
        raise FileExistsError
    os.makedirs(arg.out_dir)

def printGenerated(sequencesToSave, path):
    outFile=open(path,"w")
    for name in sequencesToSave:
        generatedFromOneSeq=sequencesToSave[name]
        for i in range(len(generatedFromOneSeq)):
            print(">",name,"| SHUFFLED SEQ NR", i+1, file=outFile)
            print(generatedFromOneSeq[i], file=outFile)

def main():
    arg,parser=parseArguments()
    prepareOutDir(arg)

    if arg.python_random:
        randomGen=random.Random()
    else:
        randomGen=random.SystemRandom()

    if arg.deterministic:
        randomGen=random.Random()
        detSeed=202012355
        if arg.verbose:
            print("Setting seed on:",detSeed)
        randomGen.seed(detSeed)
    
    if arg.nucAll:
        shuffler=dS.NucAllShuffler(arg.number_of_generated_seq,arg.start, arg.end, randomGen)
    elif arg.nucPos:
        shuffler=dS.NucPosShuffler(arg.number_of_generated_seq,arg.start, arg.end, randomGen)
    elif arg.codons:
        shuffler=dS.CodonShuffler(arg.number_of_generated_seq,arg.start, arg.end, randomGen)
    elif arg.aminoacids:
        shuffler=dS.AminoacidShuffler(arg.number_of_generated_seq,arg.start, arg.end, randomGen)
    else:
        raise RuntimeError("Function argument don't match with any programed option. Please report a bug.")
        
    for file in arg.files:
        if arg.verbose:
            print("Starting procesing file:", file)

        sequencesToSave={}
        seqDict=rsf.prepareDictionary([file])
        for seqName,seq in seqDict.items():
            generated=shuffler.generate(seq)
            if arg.distinct:
                printGenerated({seqName:generated}, os.path.join(arg.out_dir,seqName))
            else:
                sequencesToSave[seqName]=generated
        if not arg.distinct:
            printGenerated(sequencesToSave, os.path.join(arg.out_dir,os.path.basename(os.path.splitext(file)[0])))


def parseArguments():
    parser=argparse.ArgumentParser()
    
    parser.add_argument("files",nargs="+", type=str,
                        help="Files with sequences based on which should be new random sequences generated.")

    funGrp=parser.add_mutually_exclusive_group(required=True)
    funGrp.add_argument("--nucAll", action="store_true", 
            help="Shuffle sequence based on number of nucleotides in whole sequence.")
    funGrp.add_argument("--nucPos", action="store_true",
            help="Shuffle sequence based on number of nucleotides on each position.")
    funGrp.add_argument("--codons", action="store_true",
            help="Shuffle sequence based on codons in sequence.")
    funGrp.add_argument("--aminoacids", action="store_true",
            help="Shuffle sequence changing only codons in synonymous group. Sequence on aminoacids level shoud be without change.")
    
    
    parser.add_argument("-d","--deterministic", action="store_true",
            help="Don't initialize pseudo random generator with random seed.")
    parser.add_argument("-e","--end", action="store_true",
            help="Take into account when shuffling last codon. Default: preserve last codon.")
    parser.add_argument("-s","--start", action="store_true",
            help="Take into account when shuffling first codon. Default: preserve start codon.")
    parser.add_argument("-n","--number-of-generated-seq", type=int, default=1,
            help="How many shuffled sequences should be generated from one fasta sequence.")
    parser.add_argument("-od","--out_dir", type=str,default=".",
            help="Directory in which output files should be saved.")
    parser.add_argument("-v", "--verbose", action="store_true",
            help="Turn on verbose mode.")
    parser.add_argument("-t","--distinct", action="store_true",
            help="Save shuffled sequences derived from diffrent orginal sequences into distinct out files.")
    parser.add_argument("-pr","--python-random", action="store_true",
            help="Use python pseudo random number generator. Default use system /dev/urandom.")

    arg=parser.parse_args()
    return arg,parser

if __name__=="__main__":
    main()
