import numpy as np
import pomegranate as pom
import random

allCodons=frozenset({'AGC', 'CAT', 'TCG', 'GAA', 'TGG', 'TGA', 'GAT', 'CCC', 'CTT', 'CGG', 'GCT', 'TGT', 'AGA', 'TAA', 'GTA', 'CTC', 'AAC', 'CAC', 'AAA', 'CGA', 'TGC', 'TTA', 'TTG', 'ATC', 'GAG', 'TCT', 'GCA', 'GTC', 'AAT', 'GGA', 'CCA', 'TTT', 'TCA', 'GGT', 'TAT', 'TAC', 'GAC', 'GGC', 'AAG', 'CTG', 'GCG', 'AGG', 'TAG', 'GCC', 'CGT', 'ACA', 'TCC', 'TTC', 'ATG', 'CGC', 'ATA', 'GTG', 'CTA', 'GTT', 'GGG', 'ACC', 'CCG', 'AGT', 'CAA', 'CAG', 'ATT', 'CCT', 'ACT', 'ACG'})
stopCodons=frozenset({"TAG","TGA","TAA"})

def createWeightsDict(matrix):
    '''Take ndarray which describe transition matrix.'''
    N=["A","T","C","G"]
    weights={}
    for n1 in range(4):
        for n2 in range(4):
            for n3 in range(4):
                s=0
                #TAG
                s+=matrix[n1,1]*matrix[n2,0]*matrix[n3,3]
                #TGA
                s+=matrix[n1,1]*matrix[n2,3]*matrix[n3,0]
                #TAA
                s+=matrix[n1,1]*matrix[n2,0]*matrix[n3,0]
                codon=N[n1]+N[n2]+N[n3]
                weights[codon]=s
    return weights


def loadSequencesIntoDict(dictionary,file):
    '''Take handle file to fasta file and load all seqences from it into dictionary.'''
    #print(dictionary)
    for line in file:
        line=line.strip()
        #If line is empty
        if line==False:
            contine
        if line[0]==">":
            currentSequence=line[1:]
            if currentSequence in dictionary:
                #print(dictionary)
                raise RuntimeError()
            dictionary[currentSequence]=""
        else:
            line=line.replace(" ","")
            dictionary[currentSequence]=dictionary[currentSequence]+line


# ## Functions on sequences

def lenToThreeDivisable(sequence):
    remind=len(sequence)%3
    if remind==1:
        return ""
    if remind==2:
        return ""
    return sequence


def sequenceToTypeList(sequence):
    '''Take sequence with proper length (divisable by 3)
    and return list with type of each codon.'''
    typeList=[-1]*int(len(sequence)/3)
    for codonStart in range(0,len(sequence),3):
        codon=sequence[codonStart:codonStart+3]
        for codonGroup in usedCoding.coding:
            if codon in codonGroup:
                typeList[int(codonStart/3)]=usedCoding.coding[codonGroup]
                break
    return typeList


def removeStopTypeFromSeq(seq):
    '''Take sequence in form of string and remove all non-last stop codons,
    if there is no last codon then add "TAG".'''
    if len(seq)<3:
        return ""
    #Removel all last codons, which isn't last
    if seq[-3:] not in stopCodons:
        return ""
    while seq[-6:-3] in stopCodons:
        seq=seq[:-3]

    for i in range(0,len(seq)-3,3):
        if seq[i:i+3] in stopCodons:
            return ""
    return seq


def removeNonCorrectCodons(sequence):
    '''Take sequence and remove all non-corect codons.'''
    for codonStart in range(0,len(sequence),3):
        codon=sequence[codonStart:codonStart+3]
        if codon not in allCodons:
            return ""
    return sequence


# ## Functions on dictionary with sequences

def removeEmptyFromDictionary(seqDict):
    '''Take dictionary and remove all empty sequence.'''
    toRemove=[]
    for seqName in seqDict:
        if not seqDict[seqName]:
            toRemove.append(seqName)
    while len(toRemove)>0:
        del seqDict[toRemove[-1]]
        toRemove.pop()
    return seqDict


def cleanDictionary(seqDict):
    '''Each sequence in dictionary is shrinked to length
    which is divisable by three,non-correct codons are
    removed and empty sequences are removed.'''
    toRemove=[]
    for seqName in seqDict:
        seqDict[seqName]=lenToThreeDivisable(seqDict[seqName])
        seqDict[seqName]=removeNonCorrectCodons(seqDict[seqName])
    seqDict=removeEmptyFromDictionary(seqDict)
    return seqDict


def createDictionaryWithListsOfCodonsType(seqDict):
    '''Take dictionary with sequences and return
    dictionary with lists of codons type.'''
    listDict={}
    for seqName in seqDict:
        listDict[seqName]=sequenceToTypeList(seqDict[seqName])
    return listDict


def removeStopsFromDictionary(seqDict):
    '''Take dictionary with sequences in strings
    and remove all stops.'''
    for seqName in seqDict:
        seqDict[seqName]=removeStopTypeFromSeq(seqDict[seqName])


def convertListDictionaryIntoNdarrayDictionary(seqDict):
    for seqName in seqDict:
        seqDict[seqName]=np.array(seqDict[seqName])


def prepareDictionary(paths):
    """Construct dictionaries with sequences and coded sequences from fasta files"""
    seqDict={}
    for p in paths:
        file=open(p,"r")
        loadSequencesIntoDict(seqDict,file)
        file.close()
    seqDict=cleanDictionary(seqDict)
    removeStopsFromDictionary(seqDict)
    removeEmptyFromDictionary(seqDict)
    return seqDict

def traindAndTestDataset(paths, testSize=50, generator=None):
    """ Construct datasets base on fasta files.
    Arguments:
        paths - list of paths to fasta files
        testSize=50 - size of list with test sequences
        generator=None - numpy random number generator, which will be used to choose sequences to test list
    Return:
        seqDict - dictionary with names from fasta file as keys and list of nucleotides as values
        seqDictT -dictionary with names from fasta file as keys and list of coded codons as values
        trainList - list with sequences (coded codons) choosed to train list
        trainNamesList - list of names of sequences choosed to train list. Name i is name of sequence i from trainList
        testList - list with sequences (coded codons) choosed to test list
        testNamesList - list of names of sequences choosed to test list. Name i is name of sequence i from testList
    """
    seqDict,seqDictT=prepareDictionary(paths)

    seqList=list(seqDictT.values())
    seqNamesList=list(seqDictT.keys())

    if generator is not None:
        indTest=generator.choice(range(len(seqList)),size=testSize,replace=False)
    else:
        indTest=np.random.choice(range(len(seqList)),size=testSize,replace=False)
    indTrain=np.delete(np.arange(len(seqList)), indTest)

    testList=[seqList[i] for i in indTest]
    testNamesList=[seqNamesList[i] for i in indTest]
    trainList=[seqList[i] for i in indTrain]
    trainNamesList=[seqNamesList[i] for i in indTrain]
    return (seqDict,seqDictT,trainList,trainNamesList,testList,testNamesList)


