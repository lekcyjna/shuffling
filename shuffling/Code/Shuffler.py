class Shuffler():
    def __init__(self,numberOfGeneratedSeq:int, start:bool, end:bool, randomGen):
        self.numberOfGeneratedSeq=numberOfGeneratedSeq
        self.start=start
        self.end=end
        self.randomGen=randomGen

    def _shufflingAlg(self, seq):
        """
        Take string of elements to shuffle and return shuffled string.
        """
        raise NotImplementedError("This function should be overwritten by derived class.")

    def __generateSeqToShuffle(self, seq):
        if not self.start and not self.end:
            seqToShuffle=seq[3:-3]
        elif self.start and self.end:
            seqToShuffle=seq
        elif not self.start:
            seqToShuffle=seq[3:]
        else:
            seqToShuffle=seq[:-3]
        return seqToShuffle

    def __postprocessShuffledSeq(self, seq, seqShuffled):
        if not self.start and not self.end:
            seqGen=seq[0:3]+seqShuffled+seq[-3:]
        elif self.start and self.end:
            seqGen=seqShuffled
        elif not self.start:
            seqGen=seq[0:3]+seqShuffled
        else:
            seqGen=seqShuffled+seq[-3:]
        return seqGen


    def generate(self, seq):
        """
        Take sequence as string and return list of shuffled sequences as strings.
        """
        seqToShuffle=self.__generateSeqToShuffle(seq)
        generated=[]
        for i in range(self.numberOfGeneratedSeq):
            seqShuffled=self._shufflingAlg(seqToShuffle)
            generated.append(self.__postprocessShuffledSeq(seq,seqShuffled))
        return generated

