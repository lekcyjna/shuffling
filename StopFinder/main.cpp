#include <iostream>
#include <fstream>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include <cctype>
#include <tuple>

// Program write to the output positions of the first nucleotide from stop codon
// First nucleotide in sequence has number 0.

///Codons which we are looking for
std::unordered_set<std::string> stopsCodons={"TAA","TAG","TGA"};

class FoundInPhase
{
  std::vector<int> positionInNucleotides;
  std::vector<std::string> type;

  public:
  ///put stop codon
  void put(std::string sequence, int pos);

  ///pop pair positionInNucleotides and stop codon
  std::pair<int,std::string> operator[](const unsigned int pos) const;
  std::pair<int,std::string> back() const;

  void pop_back();
  int size() const;
};
void FoundInPhase::put(std::string sequence, int pos)
{
  positionInNucleotides.push_back(pos);
  type.push_back(sequence);
}
std::pair<int,std::string> FoundInPhase::operator[](const unsigned int pos) const
{
  return std::make_pair(positionInNucleotides[pos],type[pos]);
}
std::pair<int,std::string> FoundInPhase::back() const
{
  return std::make_pair(positionInNucleotides.back(),type.back());
}
void FoundInPhase::pop_back()
{
  positionInNucleotides.pop_back();
  type.pop_back();
  return;
}
int FoundInPhase::size() const
{
  return positionInNucleotides.size();
}

void clearSequence(std::string sequence, std::string& result)
{
  // Remove white spaces from input sequence
  int seqSize=sequence.size();

  result.resize(0);
  result.resize(seqSize);

  int j=0;
  for(int i=0;i<seqSize;i++)
  {
    if(!std::isspace(sequence[i]))
    {
      result[j]=sequence[i];
      j++;
    }
  }
  result.resize(j);
  result.shrink_to_fit();
  return;
}

void findStopCodons(std::vector<FoundInPhase>& stopsInPhases, std::string & sequence)
{
  int seqSize=sequence.size();
  char codon[4]={'!',sequence[0],sequence[1], '\0'};
  for(int i=2;i<seqSize;i++)
  {
    codon[0]=codon[1];
    codon[1]=codon[2];
    codon[2]=sequence[i];
    const std::string codonStr(codon);
    if(1==stopsCodons.count(codonStr))
    {
      //i-2 because i is position of last nucleotide
      //and we will check position of first nucleotide
      stopsInPhases[(i-2)%3].put(codonStr, i-2);
    }
  }
}

int main(int argc, char** argv)
{
  //Check if we have argument with file path
  if (argc<2)
  {
    std::cerr<<"Not enough arguments.\n";
    return 1;
  }

  //Open input and output files
  std::string pathToFile=argv[1];

  std::ifstream inputFile;
  inputFile.open(pathToFile);

  std::ofstream outputFiles[3];
  outputFiles[0].open(pathToFile+"-f0.txt");
  outputFiles[1].open(pathToFile+"-f1.txt");
  outputFiles[2].open(pathToFile+"-f2.txt");

  //Create data structures
  std::string header,sequenceFromFile,sequence;
  std::vector<std::vector<FoundInPhase> > foundedStopsCodons;
  std::vector<std::string> listOfHeaders;
  std::vector<int> listOfSeqLength;
  std::vector<std::string> listOfLast3Nucleotides;
  ///Vector of strings in which we remember lasts nucleotides
  ///according to phase, so one string can have 1,2, or 3 nucleotides
  std::vector<std::vector<std::string> > listOfLastPC;


  //Remove all garbage until first >
  std::getline(inputFile,header,'>');

  //Process each sequence
  while(!inputFile.eof())
  {
    //Get header and sequence with whitespaces
    std::getline(inputFile,header,'\n');
    std::getline(inputFile,sequenceFromFile,'>');

    if(*(header.end()-1)=='\r')
    {
      header.erase(header.end()-1);
    }

    //Save header
    listOfHeaders.push_back(header);

    //Remove white spaces from sequence
    clearSequence(sequenceFromFile,sequence);

    //Save length of sequence
    listOfSeqLength.push_back(sequence.size());

    //Check if sequence has enought length.
    if(sequence.size()<3)
    {
      std::cerr<<"WARNING: Sequence is too short: "<<header<<"\n";
      listOfLast3Nucleotides.push_back("!!!");
      listOfLastPC.push_back(std::vector<std::string> {"!!!","!!!","!!!"});
    }
    else
    {
      //Save 3 last nucleotides
      listOfLast3Nucleotides.push_back(sequence.substr(sequence.size()-3));

      //Save last nucleotides taking account on phase
      std::vector<std::string> lastInPhases;
      int reminder=sequence.size()%3;
      int offsetP0, offsetP1, offsetP2;
      switch(reminder)
      {
        case 0:
          offsetP0=3;
          offsetP1=2;
          offsetP2=1;
          break;
        case 1:
          offsetP0=1;
          offsetP1=3;
          offsetP2=2;
          break;
        case 2:
          offsetP0=2;
          offsetP1=1;
          offsetP2=3;
          break;
      }
      lastInPhases.push_back(sequence.substr(sequence.size()-offsetP0));
      lastInPhases.push_back(sequence.substr(sequence.size()-offsetP1));
      lastInPhases.push_back(sequence.substr(sequence.size()-offsetP2));

      listOfLastPC.push_back(lastInPhases);
    }
    //Create vector in which we will remember 
    //positions of stop codons in appropriate phase
    std::vector<FoundInPhase> stopsInThisSequence;
    stopsInThisSequence.resize(3);

    //Find stops codons
    findStopCodons(stopsInThisSequence, sequence);

    //Save positions of stops codons.
    foundedStopsCodons.push_back(stopsInThisSequence);
  }

  for(unsigned int i=0;i<foundedStopsCodons.size();i++)
  {
    //Print sequence names
    for(int j=0;j<3;j++)
    {
      outputFiles[j]<<listOfHeaders[i]<<'\t'<<listOfSeqLength[i]<<"\t"
        <<listOfLast3Nucleotides[i]<<"\t"<<listOfLastPC[i][j]<<"\t";
    }

    std::vector<FoundInPhase> codonsLists=foundedStopsCodons[i];

    //If there is no stops codons in sequence
    //print warning
    if(codonsLists[0].size()==0)
    {
      if(codonsLists[1].size()==0 && codonsLists[2].size()==0)
      {
        std::cerr<<"WARNING: There is no stop in sequence: "<< listOfHeaders[i]<<"\n";
      }
      else
      {
        std::cerr<<"WARNING: There is no stop in phase 0 in sequence: "<<listOfHeaders[i]<<"\n";
      }
    }

    //Print founded stops
    if(codonsLists[0].size()!=0)
    {
      outputFiles[0]<<codonsLists[0].back().first<<"\t"<<codonsLists[0].back().second<<"\t";
      codonsLists[0].pop_back();
    }
    for(int j=0;j<3;j++)
    {
      for(int k=0;k<codonsLists[j].size();k++)
      {
        outputFiles[j]<<codonsLists[j][k].first<<"\t"<<codonsLists[j][k].second<<"\t";
      }
    }
    //Print end of line
    for(int j=0;j<3;j++)
    {
      outputFiles[j]<<'\n';
    }
  }

  inputFile.close();
  for(int i=0;i<3;i++)
  {
    outputFiles[i].close();
  }
  return 0;
}
